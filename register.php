<?php
	include 'functions.php';
	
	$name = $_GET['name'];
	$email = $_GET['email'];
	$gcm_id = $_GET['gcm_id'];
	$mobile = $_GET['mobile'];
	$model = $_GET['model'];
	
	if (isset($name) && isset($email)
		&& isset($gcm_id) && isset($mobile)
		&& isset($model)) {
		$result = saveUserToDB($gcm_id, $name, $email, $mobile, $model);
		$response = array();
		if ($result) {
			$row = mysqli_fetch_array($result);
			$response[] = array(
				'id' => $row['id'],
				'gcm_regid' => $row['gcm_regid'],
				'user_key' => $row['user_key'],
				'name' => $row['name'],
				'email' => $row['email'],
				'mobile_no' => $row['mobile_no'],
				'model' => $row['model'],
				'timestamp' => $row['timestamp'],
				'status' => 'OK'
			);
		} else {
			$response[] = array(
				'status' => 'error'
			);
		}
		$json = json_encode($response);
		print ($json);
	}
?>