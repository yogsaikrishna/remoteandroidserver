<?php
	include 'functions.php';
	
	$user_key = $_GET['user_key'];
	$command = $_GET['command'];
	$sender_key = $_GET['sender_key'];
	
	if (isset($user_key)
		&& isset($command)
		&& isset($sender_key)) {
		$receiver = fetchUserFromDB($user_key);
		$sender = fetchUserFromDB($sender_key);
		$response = null;
		if ($receiver && $sender) {
			$result_one = mysqli_fetch_array($receiver);
			$result_two = mysqli_fetch_array($sender);
			$gcm_id = $result_one['gcm_regid'];
			$sender_id = $result_two['gcm_regid'];
			$sender_key = $result_two['user_key'];
			$sender_name = $result_two['name'];
			$model = $result_two['model'];
			if ($gcm_id != null) {
				$registration_ids = array($gcm_id);
				$data = array("sender" => $sender_name,
					"sender_key" => $sender_key,
					"sender_id" => $sender_id,
					"model" => $model,
					"command" => $command);
				$response = sendCommand($registration_ids, $data);
				print $response;
			}
		}		
	}
?>