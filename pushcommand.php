<?php
	include 'functions.php';
	
	$user_key = $_GET['user_key'];
	$command = $_GET['command'];
	$receiver_id = $_GET['sender_id'];
	
	if (isset($user_key)
		&& isset($command)
		&& isset($receiver_id )) {
		$response = null;
		$registration_ids = array($receiver_id);
		$data = array("sender_key" => $user_key,
				"command" => $command);
		$response = sendCommand($registration_ids, $data);
		print $response;
	}
?>