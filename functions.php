<?php
	function saveUserToDB($gcm_id, $name, $email, $mobile_no, $model) {
		include 'dbconfig.php';
		$connection = mysqli_connect($dbhost, $dbuser, $dbpassword, $dbname) or die(mysqli_error());
		$query = "INSERT INTO rm_users (gcm_regid, name, email, mobile_no, model) VALUES ('$gcm_id', '$name', '$email', '$mobile_no', '$model')";
		$result = mysqli_query($connection, $query);
		if ($result) {
			$id = mysqli_insert_id($connection);
			$result = mysqli_query($connection, "SELECT * FROM rm_keys WHERE id = " . $id);
			if (mysqli_num_rows($result) > 0) {
				$array = mysqli_fetch_array($result);
				$user_key = $array['key'];
				if ($user_key != null) {
					mysqli_query($connection, "UPDATE rm_users SET user_key = " . $user_key . " WHERE id = ". $id);
					mysqli_query($connection, "DELETE FROM rm_keys WHERE id = " . $id);
				}
			}
			$result = mysqli_query($connection, "SELECT * FROM rm_users WHERE id = " . $id);
			if (mysqli_num_rows($result) > 0) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	function fetchUserFromDB($user_key) {
		include 'dbconfig.php';
		$connection = mysqli_connect($dbhost, $dbuser, $dbpassword, $dbname) or die(mysqli_error());
		$result = mysqli_query($connection, "SELECT * FROM rm_users WHERE user_key = " . $user_key);
		if (mysqli_num_rows($result) > 0) {
			return $result;
		} else {
			return false;
		}
	}
	
	function sendCommand($registration_ids, $command) {
		include 'dbconfig.php';
		$fields = array(
			'registration_ids' => $registration_ids,
			'data' => $command);
		$headers = array(
			'Authorization: key=' . $api_key,
			'Content-Type: application/json');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
?>